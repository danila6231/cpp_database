Implementation of a simple database in C++.

Possible commands:

 - Add date event — insert the pair (date, event) into the database;
 - Print — list the contents of the database;
 - Find condition — print all records contained in the database that satisfy the condition;
 - Del condition — remove from the database all records that satisfy the condition;
 - Last date — display a record with the last event that happened no later than the given date.
